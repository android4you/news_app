

class GeneralResponse {
  Response response;

  GeneralResponse({this.response});

  GeneralResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['response'] = this.response.toJson();
    }
    return data;
  }
}

class Response {
  String status;
  String userTier;
  int total;
  int startIndex;
  int pageSize;
  int currentPage;
  int pages;
  String orderBy;
  List<Results> results;

  Response(
      {this.status,
        this.userTier,
        this.total,
        this.startIndex,
        this.pageSize,
        this.currentPage,
        this.pages,
        this.orderBy,
        this.results});

  Response.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    userTier = json['userTier'];
    total = json['total'];
    startIndex = json['startIndex'];
    pageSize = json['pageSize'];
    currentPage = json['currentPage'];
    pages = json['pages'];
    orderBy = json['orderBy'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['userTier'] = this.userTier;
    data['total'] = this.total;
    data['startIndex'] = this.startIndex;
    data['pageSize'] = this.pageSize;
    data['currentPage'] = this.currentPage;
    data['pages'] = this.pages;
    data['orderBy'] = this.orderBy;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  String id;
  String type;
  String sectionId;
  String sectionName;
  String webPublicationDate;
  String webTitle;
  String webUrl;
  String apiUrl;
  Fields fields;
  List<Tags> tags;
  bool isHosted;
  String pillarId;
  String pillarName;

  Results(
      {this.id,
        this.type,
        this.sectionId,
        this.sectionName,
        this.webPublicationDate,
        this.webTitle,
        this.webUrl,
        this.apiUrl,
        this.fields,
        this.tags,
        this.isHosted,
        this.pillarId,
        this.pillarName});

  Results.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    sectionId = json['sectionId'];
    sectionName = json['sectionName'];
    webPublicationDate = json['webPublicationDate'];
    webTitle = json['webTitle'];
    webUrl = json['webUrl'];
    apiUrl = json['apiUrl'];
    fields =
    json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
    if (json['tags'] != null) {
      tags = new List<Tags>();
      json['tags'].forEach((v) {
        tags.add(new Tags.fromJson(v));
      });
    }
    isHosted = json['isHosted'];
    pillarId = json['pillarId'];
    pillarName = json['pillarName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['sectionId'] = this.sectionId;
    data['sectionName'] = this.sectionName;
    data['webPublicationDate'] = this.webPublicationDate;
    data['webTitle'] = this.webTitle;
    data['webUrl'] = this.webUrl;
    data['apiUrl'] = this.apiUrl;
    if (this.fields != null) {
      data['fields'] = this.fields.toJson();
    }
    if (this.tags != null) {
      data['tags'] = this.tags.map((v) => v.toJson()).toList();
    }
    data['isHosted'] = this.isHosted;
    data['pillarId'] = this.pillarId;
    data['pillarName'] = this.pillarName;
    return data;
  }
}

class Fields {
  String trailText;
  String thumbnail;

  Fields({this.trailText, this.thumbnail});

  Fields.fromJson(Map<String, dynamic> json) {
    trailText = json['trailText'];
    thumbnail = json['thumbnail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['trailText'] = this.trailText;
    data['thumbnail'] = this.thumbnail;
    return data;
  }
}

class Tags {
  String id;
  String type;
  String webTitle;
  String webUrl;
  String apiUrl;

  String bio;
  String firstName;
  String lastName;
  String bylineImageUrl;
  String bylineLargeImageUrl;
  String twitterHandle;

  Tags(
      {this.id,
        this.type,
        this.webTitle,
        this.webUrl,
        this.apiUrl,
        this.bio,
        this.firstName,
        this.lastName,
        this.bylineImageUrl,
        this.bylineLargeImageUrl,
        this.twitterHandle});

  Tags.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    webTitle = json['webTitle'];
    webUrl = json['webUrl'];
    apiUrl = json['apiUrl'];

    bio = json['bio'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    bylineImageUrl = json['bylineImageUrl'];
    bylineLargeImageUrl = json['bylineLargeImageUrl'];
    twitterHandle = json['twitterHandle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['webTitle'] = this.webTitle;
    data['webUrl'] = this.webUrl;
    data['apiUrl'] = this.apiUrl;
    data['bio'] = this.bio;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['bylineImageUrl'] = this.bylineImageUrl;
    data['bylineLargeImageUrl'] = this.bylineLargeImageUrl;
    data['twitterHandle'] = this.twitterHandle;
    return data;
  }
}