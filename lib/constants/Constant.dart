
String
ANIMATED_SPLASH = '/SplashScreen',
    HOME_SCREEN = '/HomeScreen',
    SETTINGS_CONTAINER_SCREEN = '/SettingsScreen',
    TVSHOW_CONTAINER_SCREEN = '/TVShowScreen',
    PEOPLE_CONTAINER_SCREEN = '/PeopleScreen',
    DETAILS_CONTAINER_SCREEN = '/DetailsScreen'
;
