

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_app/data/model/GeneralResponse.dart';
import 'package:rxdart/rxdart.dart';

import 'HomeBloc.dart';
import 'NewListView.dart';


class NewsScreen extends StatefulWidget {

 final int data;

  NewsScreen({Key key, this.data});
  @override
  _NewsScreenState createState() => _NewsScreenState();
}


class _NewsScreenState extends State<NewsScreen> {
  HomeBloc bloc;

  @override
  void initState() {
    bloc = HomeBloc();
    bloc.getUpcomingMovies(widget.data);

  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _buildPeopleListView();
  }


  Widget _buildPeopleListView() {
    return Container(
      child: Column(
        children: <Widget>[
          StreamBuilder(
            stream: getData(),
            builder: (context, AsyncSnapshot<Response> snapshot) {
              return NewListView(listItems: snapshot);
            },
          ),
        ],
      ),
    );
  }

  Observable<Response> getData(){
    return  bloc.newsList;
  }



}
