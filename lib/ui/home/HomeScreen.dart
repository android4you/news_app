import 'package:flutter/material.dart';
import 'package:news_app/ui/home/NewsScreen.dart';

import 'AppDrawer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: DefaultTabController(
            length: 9,
            child: Scaffold(
                appBar: AppBar(
                  backgroundColor: Colors.blue,
                  title: Text(
                    "New Headlines",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  bottom: TabBar(
                    isScrollable: true,
                    tabs: [
                      Tab(
                        text: "HOME",
                      ),
                      Tab(text: "WORLD"),
                      Tab(text: "SCIENCE"),
                      Tab(text: "SPORTS"),
                      Tab(text: "ENVIRONMENT"),
                      Tab(text: "SOCIETY"),
                      Tab(text: "FASHION"),
                      Tab(text: "BUSINESS"),
                      Tab(text: "CULTURE"),
                    ],
                  ),
                ),
                drawer: AppDrawer(),
                body: TabBarView(children: [
                  NewsScreen(data: 0,),
                  NewsScreen(data: 1,),
                  NewsScreen(data: 2),
                  NewsScreen(data: 3,),
                  NewsScreen(data: 4,),
                  NewsScreen(data: 5,),
                  NewsScreen(data: 6),
                  NewsScreen(data: 7,),
                  NewsScreen(data: 8,),
                ],
                  ))));
  }
}
