import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:news_app/data/model/GeneralResponse.dart';

class NewListView extends StatelessWidget {
  final AsyncSnapshot<Response> listItems;

  const NewListView({Key key, this.listItems}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Populate list view with the data
    if (listItems.hasData) {
      return _buildListView(context, listItems.data);
    }
    // Display loading indicator
    else if (listItems.hasError) {
      return Center(child: Text(listItems.error.toString()));
    } else {
      return Container(
        height: 300,
        color: Colors.transparent,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
  }

  Widget _buildListView(BuildContext context, Response movies) {
    return Expanded(
      child: Container(
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: movies.results.length,
          itemBuilder: (BuildContext context, int index) {
            return buildPeopleItemView(context, movies.results[index]);
          },
        ),
      ),
    );
  }
}

Widget buildPeopleItemView(BuildContext context, Results result) {
  return InkWell(
    child: Card(
        color: Colors.white,
        margin: EdgeInsets.all(10),
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                result.webTitle,
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.black87,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Row(
              children: [
                Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(result.sectionName,
                      style: TextStyle(fontSize: 14,
                      color: Colors.blue),),
                      Html(data: result.fields.trailText),
                      SizedBox(
                        height: 15,
                      ),

                    ],
                  ),

                )),
                imageViewset(result.fields.thumbnail)

              ],
            )
          ],
        )),
    onTap: () => {},
  );



}



Widget imageViewset(String url){
  if(url!= null) {
    return Image.network(
    url,
    fit: BoxFit.cover,
    width: 150,
    // errorBuilder: (context, error, stackTrace) {
    //   print(error); //do something
    // },
    // loadingBuilder: (BuildContext context, Widget child,
    //     ImageChunkEvent loadingProgress) {
    //   if (loadingProgress == null) return child;
    //   return Center(
    //     child: CircularProgressIndicator(
    //       value: loadingProgress.expectedTotalBytes != null
    //           ? loadingProgress.cumulativeBytesLoaded /
    //           loadingProgress.expectedTotalBytes
    //           : null,
    //     ),
    //   );
    // },
  );
}else{
  return  Text("");
  }

}