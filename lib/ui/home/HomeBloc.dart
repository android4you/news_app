
import 'package:news_app/data/model/GeneralResponse.dart';
import 'package:news_app/repository/Repository.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc  {

  final repository = Repository();

  final newsFetcher = PublishSubject<Response>();



  Observable<Response> get newsList => newsFetcher.stream;


  @override
  dispose() {
    newsFetcher.close();

  }

  void getUpcomingMovies(int type) async {
    Response movieList = await repository.getUpcomingNews(type);
    newsFetcher.sink.add(movieList);
  }

}

final homeBloc = HomeBloc();
