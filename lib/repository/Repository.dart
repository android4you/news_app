
import 'package:news_app/data/model/GeneralResponse.dart';
import 'package:news_app/data/remote/APIService.dart';

class Repository {
  final apiProvider = APIService();

  Future<Response> getUpcomingNews(int type) =>
      apiProvider.getMovies(type);


}
